<?php

namespace App\Http\Controllers\Admin;

use App\Ebook;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Html\Builder;

class EbookController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth_admin', 'can:Access Admin Panel']);
        $this->middleware('intend_url')->only(['index', 'read']);
        $this->middleware('can:Create Ebooks')->only(['createForm', 'create']);
        $this->middleware('can:Read Ebooks')->only(['index', 'read']);
        $this->middleware('can:Update Ebooks')->only(['updateForm', 'update']);
        $this->middleware(['can:Delete Ebooks'])->only('delete');
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $ebooks = Ebook::query();
            $datatable = datatables($ebooks)
                ->editColumn('created_by', function ($ebook) {
                    return $ebook->creator->email;
                })
                ->editColumn('actions', function ($ebook) {
                    return view('admin.ebooks.datatable.actions', compact('ebook'));
                })
                ->rawColumns(['actions']);

            return $datatable->toJson();
        }

        $html = $builder->columns([
            ['title' => 'Title', 'data' => 'title'],
            ['title' => 'Body', 'data' => 'body'],
            ['title' => 'Creator', 'data' => 'created_by'],
            ['title' => '', 'data' => 'actions', 'searchable' => false, 'orderable' => false],
        ]);
        $html->setTableAttribute('id', 'ebooks_datatable');

        return view('admin.ebooks.index', compact('html'));
    }

    public function createForm()
    {
        $string = 'hello world';
        $html = '<strong>ohaiyo</strong>';
        $droplist = [
            'a',
            'b',
            'c',
        ];
        $droplist2 = [
        ];
        return view('admin.ebooks.create')->with(compact('droplist','droplist2','string','html'));
    }

    public function create()
    {
        $this->validate(request(), [
            "title" => "required",
            "body" => "required",
            // "created_by" => "required",
        ]);
        request()->merge(['created_by' => auth()->user()->id]);
        $ebook = Ebook::create(request()->all());

        activity('Created Ebook: ' . $ebook->id, request()->all(), $ebook);
        flash(['success', 'Ebook created!']);

        if (request()->input('_submit') == 'redirect') {
            return response()->json(['redirect' => session()->pull('url.intended', route('admin.ebooks'))]);
        }
        else {
            return response()->json(['reload_page' => true]);
        }
    }

    public function read(Ebook $ebook)
    {
        return view('admin.ebooks.read', compact('ebook'));
    }

    public function updateForm(Ebook $ebook)
    {
        return view('admin.ebooks.update', compact('ebook'));
    }

    public function update(Ebook $ebook)
    {
        $this->validate(request(), [
            "title" => "required",
            "body" => "required",
            "created_by" => "required",
        ]);

        $ebook->update(request()->all());

        activity('Updated Ebook: ' . $ebook->id, request()->all(), $ebook);
        flash(['success', 'Ebook updated!']);

        if (request()->input('_submit') == 'redirect') {
            return response()->json(['redirect' => session()->pull('url.intended', route('admin.ebooks'))]);
        }
        else {
            return response()->json(['reload_page' => true]);
        }
    }

    public function delete(Ebook $ebook)
    {
        $ebook->delete();

        activity('Deleted Ebook: ' . $ebook->id, $ebook->toArray());
        $flash = ['success', 'Ebook deleted!'];

        if (request()->input('_submit') == 'reload_datatables') {
            return response()->json([
                'flash' => $flash,
                'reload_datatables' => true,
            ]);
        }
        else {
            flash($flash);

            return redirect()->route('admin.ebooks');
        }
    }
}