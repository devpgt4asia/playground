<?php

namespace App\Http\Controllers\Admin;

use App\Member;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Html\Builder;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth_admin', 'can:Access Admin Panel']);
        $this->middleware('intend_url')->only(['index', 'read']);
        $this->middleware('can:Create Members')->only(['createForm', 'create']);
        $this->middleware('can:Read Members')->only(['index', 'read']);
        $this->middleware('can:Update Members')->only(['updateForm', 'update']);
        $this->middleware(['can:Delete Members'])->only('delete');
    }

    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $members = Member::query();
            $datatable = datatables($members)
                ->editColumn('actions', function ($member) {
                    return view('admin.members.datatable.actions', compact('member'));
                })
                ->rawColumns(['variants','actions']);

            return $datatable->toJson();
        }

        $html = $builder->columns([
            ['title' => 'Name', 'data' => 'name'],
            ['title' => 'Email', 'data' => 'email'],
            ['title' => 'Contact', 'data' => 'contact_number'],
            ['title' => '', 'data' => 'actions', 'searchable' => false, 'orderable' => false],
        ]);
        $html->setTableAttribute('id', 'members_datatable');

        return view('admin.members.index', compact('html'));
    }

    public function createForm()
    {
        return view('admin.members.create');
    }

    public function create()
    {
        $this->validate(request(), [
            "name" => "required",
            "contact_number" => "required",
            "password" => "required",
            "email" => "required|email",
        ]);
        $requests = request()->all();
        $requests['password'] = bcrypt($requests['password']);

        $member = Member::create($requests);

        activity('Created Member: ' . $member->id, request()->all(), $member);
        flash(['success', 'Member created!']);

        if (request()->input('_submit') == 'redirect') {
            return response()->json(['redirect' => session()->pull('url.intended', route('admin.members'))]);
        }
        else {
            return response()->json(['reload_page' => true]);
        }
    }

    public function read(Member $member)
    {
        return view('admin.members.read', compact('member'));
    }

    public function updateForm(Member $member)
    {
        return view('admin.members.update', compact('member'));
    }

    public function update(Member $member)
    {
        $this->validate(request(), [
            "name" => "required",
            "contact_number" => "required",
            "email" => "required|email",
        ]);
        $requests = request()->all();
        if (isset($requests['password']) && $requests['password'] == '') {
            unset($requests['password']);
        } else {
            $requests['password'] = bcrypt($requests['password']);
        }

        $member->update($requests);

        activity('Updated Member: ' . $member->id, request()->all(), $member);
        flash(['success', 'Member updated!']);

        if (request()->input('_submit') == 'redirect') {
            return response()->json(['redirect' => session()->pull('url.intended', route('admin.members'))]);
        }
        else {
            return response()->json(['reload_page' => true]);
        }
    }

    public function delete(Member $member)
    {
        $member->delete();

        activity('Deleted Member: ' . $member->id, $member->toArray());
        $flash = ['success', 'Member deleted!'];

        if (request()->input('_submit') == 'reload_datatables') {
            return response()->json([
                'flash' => $flash,
                'reload_datatables' => true,
            ]);
        }
        else {
            flash($flash);

            return redirect()->route('admin.members');
        }
    }
}