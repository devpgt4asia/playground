<?php
namespace App\Http\Controllers\Admin;

use Kjjdion\LaravelAdminPanel\Controllers\UserController as LAPUserController;
use Yajra\DataTables\Html\Builder;
use Illuminate\Support\Str;

class UserController extends LAPUserController
{
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $users = app(config('auth.providers.users.model'))->with('roles');
            $datatable = datatables($users)
                ->editColumn('roles', function ($user) {
                    return $user->roles->sortBy('name')->implode('name', ', ');
                })
                ->editColumn('actions', function ($user) {
                    return view('lap::users.datatable.actions', compact('user'));
                })
                ->rawColumns(['actions']);

            return $datatable->toJson();
        }

        $html = $builder->columns([
            ['title' => 'Name', 'data' => 'name'],
            ['title' => 'Email Address', 'data' => 'email'],
            ['title' => 'Roles', 'data' => 'roles', 'searchable' => false, 'orderable' => false],
            ['title' => 'Api Token', 'data' => 'api_token'],
            ['title' => '', 'data' => 'actions', 'searchable' => false, 'orderable' => false],
        ]);
        $html->setTableAttribute('id', 'users_datatable');

        return view('lap::users.index', compact('html'));
    }
    public function create()
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);

        $data = array_merge(request()->all(), [
            'password' => \Hash::make(request()->input('password')),
        ]);
        $data['api_token'] = Str::uuid();

        $user = app(config('auth.providers.users.model'))->create($data);
        $user->roles()->sync(request()->input('roles'));

        activity('Created User: ' . $user->name, array_except($data, ['password', 'password_confirmation']), $user);
        flash(['success', 'User created!']);
        
        if (request()->input('_submit') == 'redirect') {
            return response()->json(['redirect' => session()->pull('url.intended', route('admin.users'))]);
        }
        else {
            return response()->json(['reload_page' => true]);
        }
    }
    public function update($id)
    {
        $this->validate(request(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
        ]);
        $requests = request()->all();
        $user = app(config('auth.providers.users.model'))->findOrFail($id);
        if ($user->api_token == null) {
            $requests['api_token'] = Str::uuid();
        }
        $user->update($requests);
        $user->roles()->sync(request()->input('roles'));

        activity('Updated User: ' . $user->name, request()->all(), $user);
        flash(['success', 'User updated!']);
        
        if (request()->input('_submit') == 'redirect') {
            return response()->json(['redirect' => session()->pull('url.intended', route('admin.users'))]);
        }
        else {
            return response()->json(['reload_page' => true]);
        }
    }
}