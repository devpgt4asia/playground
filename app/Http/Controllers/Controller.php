<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Carbon\Carbon;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function filter($filter, $object)
    {
        if ($filter) {
            foreach ($filter as $key => $value) {
                if ($value) {
                    if (preg_match('/_id$/i', $key)) {
                        $object->where($key,$value);
                    } elseif (preg_match('/_at$/i', $key)) {
                        $object->whereBetween($key,[Carbon::parse($value),Carbon::parse($value)->addDay()]);
                    } elseif (preg_match('/_at_range$/i', $key)) {
                        $value = explode(' - ', $value);
                        $object->whereBetween(str_replace('_range', '', $key),[Carbon::parse($value[0]),Carbon::parse($value[1])]);
                    } else {
                        $object->where($key,'like','%'.$value.'%');
                    }
                }
            }
        }
        request()->flashOnly('filter');
        session()->put(compact('filter'));
        return $object;
    }
}
