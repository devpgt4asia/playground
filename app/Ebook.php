<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kjjdion\LaravelAdminPanel\Traits\DynamicFillable;
use Kjjdion\LaravelAdminPanel\Traits\UserTimezone;

class Ebook extends Eloquent
{
    use SoftDeletes, DynamicFillable, UserTimezone;
    protected $dates = ['deleted_at'];

    
    public function creator()
    {
    	return $this->hasOne('App\User','id','created_by');
    }
    

    
}