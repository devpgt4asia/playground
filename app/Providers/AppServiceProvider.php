<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (env('APP_ENV') == 'production') {
            \URL::forceScheme('https');
        }

        view()->composer('*', function ($view)
        {
            if(session()->has('filter')) {
                request()->merge(['filter' => session()->get('filter')]);
                request()->flashOnly('filter');
            }
        });
    }
}
