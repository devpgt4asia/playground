<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" media="all">
	</head>
	<body class="bg-white">
		<div class="container-fluid">
			<div class="row mt-3 bg-dark p-2 ml-1 mr-1">
				<div class="col-md-1 col-sm-12 mt-1">
					<img width="100" src="data:image/png;base64, {{ base64_encode(file_get_contents(public_path('images/logo_w.png'))) }}">
				</div>
				<div class="col-md-6 col-sm-12 text-light font-weight-bold text-uppercase" style="font-size: 1.5rem;">
					Order Summary
				</div>
			</div>
			<div class="row mt-3 ml-1">
				<div class="col">
					Greetings {{ $name }},
				</div>
			</div>
			<div class="row mt-3 ml-1">
				<div class="col">
					Thanks for booking with Xtreme Park Asia (XPA)! Here is your details of your order.
				</div>
			</div>
			<div class="row mt-3 ml-1">
				<div class="col-md-8 col-sm-12">
					<div class="row text-secondary text-uppercase">
						<div class="col-md-2 mb-3">Name:</div>
						<div class="col-md-10 mb-3 text-dark">{{ $name }}</div>
					</div>
						
					<div class="row text-secondary text-uppercase">
						<div class="col-md-2 mb-3">Nationality:</div>
						<div class="col-md-10 mb-3 text-dark">{{ config('countries.'.$nationality.'.country') }}</div>
					</div>
						
					<div class="row text-secondary text-uppercase">
						<div class="col-md-2 mb-3">Email:</div>
						<div class="col-md-10 mb-3 text-dark">{{ $email }}</div>
					</div>
						
					<div class="row text-secondary text-uppercase">
						<div class="col-md-2 mb-3">Contact #:</div>
						<div class="col-md-10 mb-3 text-dark">{{ $phone }}</div>
					</div>
				</div>
			</div>
			<div class="row mt-2 ml-1">
				<div class="col-md-8 col-sm-12 border-dark border">
					<div class="row">
						<div class="col-md-4 col-sm-12">
			    			<img class="img-fluid" src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(300)->generate($booking_number_encrypted)) !!}">
						</div>
						<div class="col mt-4 mb-4">
							<div>
								<h4 class="text-uppercase">{{ $ticket_name }}</h4>
								<div class="row mt-3">
									<div class="col-md-3 text-uppercase text-secondary">Package</div>
									<div class="col-md-9 text-dark text-uppercase">{{ $ticket_name }} 1 Day Pass</div>
								</div>
								<div class="row mt-3">
									<div class="col-md-3 text-uppercase text-secondary">Booking #</div>
									<div class="col-md-9 text-dark">{{ $booking_number }}</div>
								</div>
								<div class="row mt-3">
									<div class="col-md-3 text-uppercase text-secondary">Date</div>
									<div class="col-md-9 text-dark">{{ \Carbon\Carbon::parse($booked_date)->format('D, d / M / Y') }}</div>
								</div>
								<div class="row mt-3">
									<div class="col-md-3 text-uppercase text-secondary">Units</div>
									<div class="col-md-9 text-dark">
										<div class="row">
											 @foreach ($details as $detail)
                                              @php
                                                $detail['label'] = isset($detail['label'])? str_replace('_', ' ' , trim($detail['label'])):'';
                                                $detail['pax'] = isset($detail['pax'])? trim($detail['pax']):'';
                                                $detail['amount'] = isset($detail['amount'])? number_format(trim($detail['amount']),2):0;
                                              @endphp
                                              @if ($detail['pax'] > 0)
	                                            <div class="col-md-12">
													{{ $detail['pax'] }} X {{ str_plural($detail['label'], $detail['pax']) }}
												</div>
                                              @endif
                                            @endforeach
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			    </div>
			</div>
			<div class="row mt-2">
				<div class="col-md-8 text-secondary text-right">
					Order Total: MYR {{ number_format(trim($amount),2) }}
				</div>
			</div>
			<div class="row mt-2">
				<div class="col-md-8 text-dark text-right font-italic" style="font-size: 1.5rem;">
					Amount Paid: MYR {{ number_format(trim($amount),2) }}
				</div>
			</div>
		</div>
	</body>
</html>