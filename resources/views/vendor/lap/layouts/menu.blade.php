<li{!! request()->is('admin/dashboard') ? ' class="active"' : '' !!}>
    <a href="{{ route('admin.dashboard') }}"><i class="fal fa-fw fa-tachometer mr-3"></i>Dashboard</a>
</li>
@can('Read Ebooks')
    <li{!! request()->is('admin/ebooks') ? ' class="active"' : '' !!}>
        <a href="{{ route('admin.ebooks') }}"><i class="fal fa-fw fa-link mr-3"></i>Ebooks</a>
    </li>
@endcan
@can('Read Permissions')
    <li{!! request()->is('admin/permissions') ? ' class="active"' : '' !!}>
        <a href="{{ route('admin.permissions') }}"><i class="fal fa-fw fa-key mr-3"></i>Permissions</a>
    </li>
@endcan
@can('Read Roles')
    <li{!! request()->is('admin/roles') ? ' class="active"' : '' !!}>
        <a href="{{ route('admin.roles') }}"><i class="fal fa-fw fa-shield-alt mr-3"></i>Roles</a>
    </li>
@endcan
@can('Read Users')
    <li{!! request()->is('admin/users') ? ' class="active"' : '' !!}>
        <a href="{{ route('admin.users') }}"><i class="fal fa-fw fa-user mr-3"></i>Users</a>
    </li>
@endcan
@can('Read Activity Logs')
    <li{!! request()->is('admin/activity_logs') ? ' class="active"' : '' !!}>
        <a href="{{ route('admin.activity_logs') }}"><i class="fal fa-fw fa-file-alt mr-3"></i>Activity Logs</a>
    </li>
@endcan
{{-- @can('Read Docs')
    <li{!! request()->is('admin/docs') ? ' class="active"' : '' !!}>
        <a href="{{ route('admin.docs') }}"><i class="fal fa-fw fa-book mr-3"></i>Docs</a>
    </li>
@endcan --}}
{{-- @can('Update Settings')
    <li{!! request()->is('admin/settings') ? ' class="active"' : '' !!}>
        <a href="{{ route('admin.settings') }}"><i class="fal fa-fw fa-cog mr-3"></i>Settings</a>
    </li>
@endcan --}}
{{-- @can('Read Members')
    <li{!! request()->is('admin/members') ? ' class="active"' : '' !!}>
        <a href="{{ route('admin.members') }}"><i class="fal fa-fw fa-users mr-3"></i>Members</a>
    </li>
@endcan --}}