<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}"/>
    <link href="//fonts.googleapis.com/css?family=Nunito:regular,bold" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('lap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lap/css/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lap/css/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('lap/css/easymde.min.css') }}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" />
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="{{ asset('lap/css/lap.css') }}">

    <title>@yield('title') | {{ config('app.name') }}</title>
    @stack('styles')
</head>
<body class="@yield('body-class')"{!! session('flash') ? ' data-flash-class="'.session('flash.0').'" data-flash-message="'.session('flash.1').'"' : '' !!}>

@yield('parent-content')

<div class="overlay"></div>

</body>
<script type="text/javascript" src="{{ asset('lap/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('lap/js/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('lap/js/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('lap/js/easymde.min.js') }}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.1/handlebars.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
<script src="{{ asset('lap/js/lap.js') }}"></script>
@stack('scripts')
</html>