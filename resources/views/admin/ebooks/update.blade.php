@extends('lap::layouts.auth')

@section('title', 'Update Ebook')
@section('child-content')
    <div class="row mb-3">
        <div class="col-md-auto mt-2 mt-md-0">
            <a href="{{ route('admin.ebooks') }}" class="btn btn-outline-primary"><i class="fas fa-backward"></i></a>
        </div>
        <div class="col-md">
            <h2 class="mb-0 text-light">@yield('title')</h2>
        </div>
    </div>

    <form method="POST" action="{{ route('admin.ebooks.update', $ebook->id) }}" novalidate data-ajax-form>
        @csrf
        @method('PATCH')

        <div class="list-group">
            <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="title" class="col-md-2 col-form-label">Title</label>
                    <div class="col-md-8">
                        <input type="text" name="title" id="title" class="form-control" value="{{ $ebook->title }}">
                    </div>
                </div>
            </div>

            <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="body" class="col-md-2 col-form-label">Body</label>
                    <div class="col-md-8">
                        <textarea name="body" id="body" class="form-control" rows="5">{{ $ebook->body }}</textarea>
                    </div>
                </div>
            </div>

            <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="created_by" class="col-md-2 col-form-label">Creator</label>
                    <div class="col-md-8">
                        <input type="text" name="created_by" id="created_by" class="form-control" value="{{ $ebook->created_by }}">
                    </div>
                </div>
            </div>

            <div class="list-group-item bg-light text-left text-md-right pb-1">
                <button type="submit" name="_submit" class="btn btn-primary mb-2" value="reload_page">Save</button>
                <button type="submit" name="_submit" class="btn btn-primary mb-2" value="redirect">Save &amp; Go Back</button>
            </div>
        </div>
    </form>
@endsection