@extends('lap::layouts.auth')

@section('title', 'Update Permission')
@section('child-content')
    <h2 class="text-light">@yield('title')</h2>

    <form method="POST" action="{{ route('admin.permissions.update', $permission->id) }}" novalidate data-ajax-form>
        @csrf
        @method('PATCH')

        <div class="list-group">
            <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="group" class="col-md-2 col-form-label">Group</label>
                    <div class="col-md-8">
                        <input type="text" name="group" id="group" class="form-control" value="{{ $permission->group }}">
                    </div>
                </div>
            </div>
            <div class="list-group-item">
                <div class="form-group row mb-0">
                    <label for="name" class="col-md-2 col-form-label">Name</label>
                    <div class="col-md-8">
                        <input type="text" name="name" id="name" class="form-control" value="{{ $permission->name }}">
                    </div>
                </div>
            </div>

            <div class="list-group-item bg-light text-left text-md-right pb-1">
                <button type="submit" name="_submit" class="btn btn-primary mb-2" value="reload_page">Save</button>
                <button type="submit" name="_submit" class="btn btn-primary mb-2" value="redirect">Save &amp; Go Back</button>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
<script type="text/javascript">
$(function () {
});
</script>
@endpush