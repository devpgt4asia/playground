<?php

return [

    // paths & files used for generating
    'paths' => [
        'stubs' => 'resources/stubs/crud/default', // template
        'controller' => 'app/Http/Controllers/Admin',
        'model' => 'app',
        'migrations' => 'database/migrations',
        'views' => 'resources/views/admin',
        'menu' => 'resources/views/vendor/lap/layouts/menu.blade.php',
        'routes' => 'routes/admin.php',
    ],

    // menu icon (fontawesome class)
    'icon' => 'fa-link',

    // model attributes
    'attributes' => [

        'title' => [
            'primary' => false,
            'migrations' => [
                'string:title',
            ],
            'validations' => [
                'create' => 'required',
                'update' => 'required',
            ],
            'datatable' => [
                'title' => 'Title',
                'data' => 'title',
            ],
            'input' => [
                'type' => 'text',
            ],
        ],


        'body' => [
            'primary' => false,
            'migrations' => [
                'text:body',
            ],
            'validations' => [
                'create' => 'required',
                'update' => 'required',
            ],
            'datatable' => [
                'title' => 'Body',
                'data' => 'body',
            ],
            'input' => [
                'type' => 'textarea',
            ],
        ],

    ],

];