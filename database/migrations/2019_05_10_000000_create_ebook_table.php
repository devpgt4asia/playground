<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEbookTable extends Migration
{
    public function up()
    {
        // create table
        Schema::create('ebooks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('body')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        // add permissions
        app(config('lap.models.permission'))->createGroup('Ebooks', ['Create Ebooks', 'Read Ebooks', 'Update Ebooks', 'Delete Ebooks']);
    }

    public function down()
    {
        // drop table
        Schema::dropIfExists('ebooks');

        // delete permissions
        app(config('lap.models.permission'))->where('group', 'Ebooks')->delete();
    }
}