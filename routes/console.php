<?php

use Illuminate\Foundation\Inspiring;

Artisan::command('testmail', function () {
	\Mail::raw('Test Mail Sent at '. \Carbon\Carbon::now(), function ($message) {
	    $message->to('wiki.chua@trip4asia.com');
	    $message->subject('Testing Mail');
	});
})->describe('Test Send Email');