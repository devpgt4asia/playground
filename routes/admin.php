<?php
// permissions
Route::get('permissions', 'Admin\PermissionController@index')->name('admin.permissions');
Route::get('permissions/create', 'Admin\PermissionController@createForm')->name('admin.permissions.create');
Route::post('permissions/create', 'Admin\PermissionController@create');
Route::get('permissions/read/{permission}', 'Admin\PermissionController@read')->name('admin.permissions.read');
Route::get('permissions/update/{permission}', 'Admin\PermissionController@updateForm')->name('admin.permissions.update');
Route::patch('permissions/update/{permission}', 'Admin\PermissionController@update');
Route::delete('permissions/delete/{permission}', 'Admin\PermissionController@delete')->name('admin.permissions.delete');

// members
Route::get('members', 'Admin\MemberController@index')->name('admin.members');
Route::get('members/create', 'Admin\MemberController@createForm')->name('admin.members.create');
Route::post('members/create', 'Admin\MemberController@create');
Route::get('members/read/{member}', 'Admin\MemberController@read')->name('admin.members.read');
Route::get('members/update/{member}', 'Admin\MemberController@updateForm')->name('admin.members.update');
Route::patch('members/update/{member}', 'Admin\MemberController@update');
Route::delete('members/delete/{member}', 'Admin\MemberController@delete')->name('admin.members.delete');


// ebooks
Route::get('ebooks', 'Admin\EbookController@index')->name('admin.ebooks');
Route::get('ebooks/create', 'Admin\EbookController@createForm')->name('admin.ebooks.create');
Route::post('ebooks/create', 'Admin\EbookController@create');
Route::get('ebooks/read/{ebook}', 'Admin\EbookController@read')->name('admin.ebooks.read');
Route::get('ebooks/update/{ebook}', 'Admin\EbookController@updateForm')->name('admin.ebooks.update');
Route::patch('ebooks/update/{ebook}', 'Admin\EbookController@update');
Route::delete('ebooks/delete/{ebook}', 'Admin\EbookController@delete')->name('admin.ebooks.delete');