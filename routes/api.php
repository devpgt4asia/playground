<?php

use Illuminate\Http\Request;

Route::any('api/auth', 'ApiController@auth')->name('api.auth');
Route::middleware('auth:api')->any('api/', 'ApiController@index')->name('api.verify');
